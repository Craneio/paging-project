﻿using System;
using System.Diagnostics;
using System.Security.Cryptography;
using System.Bcr

namespace NREncryption_Test
{
    class Program
    {
        public static Stopwatch stopWatchTest = new Stopwatch();
        public static string defDir = @"E:\NREncryption Test\Tables";

        protected static void Main(string[] args) // This method is for SHA-512
        {
            Console.Title = "SHA/BCRYPT || CURRENT = SHA-512"; // A title indicating the kind of cryptographic hashing algorithm used

            Console.WriteLine("<--------------------------Data input---------------------------------->"); // A line indicating that data input is occuring
            
            Console.Write("Please enter some data: "); // Tells the user that to enter some data
            string input = Console.ReadLine(); // This stores the user input in the string input
            Console.WriteLine(); // Line break

            if(input == "bcrypt") // This is used to determin if the tester wishes to switch to BCRYPT by entering "bcrypt"
            {
                BCRYPT(); // This calls by reference the private method BCRYPT
            }
            else if (input == "TEST")
            {
                TESTBENCH();
            }

            Console.WriteLine("<-------------------------Input processing-------------------------------->"); // This indicates that input processing is occuring
            Console.WriteLine(); // Line break

            stopWatchTest.Start();

            input.ToCharArray(); // This turns input into a char array so that it can be displayed individually for debugging

            Console.WriteLine("input.Length = " + input.Length); // This outputs the length of the char array input
            byte[] data = new byte[input.Length]; // This creates a byte array called data so that data can be hashed

            for (int y = 0; y < input.Length; y++) //I have used a for loop in order to cycle though every element in input so it can be put into a char array
            {
                data[y] = Convert.ToByte((input[y]));
                Console.WriteLine("Array[{0}] = " + data[y], y);
            }

            Console.WriteLine(); // Line break
            Console.WriteLine("<-------------------------Hashing-------------------------------->"); // This indicates that hashing is taking place
            Console.WriteLine(); // Line break

            string finresult = String.Empty; // I have declared a string called finresult to store the result in, it is initalised as empty
            SHA512 shaM = new SHA512Managed(); // This decalres a SHA manager, as being 512, so I can pass data to be hashed
            data = shaM.ComputeHash(data); // This tells the SHA manager to hash the data

            for(int i = 0; i < data.Length; i++) // This for loop will cycle though the array data, which has been hased, and output its element
            { // This is used for debugging
                Console.WriteLine("Array[{0}] = " +  data[i] , i);
                finresult += data[i]; // This appends finresult with data element position i
            }

            stopWatchTest.Stop();

            Console.WriteLine();
            Console.WriteLine("Time taken to compute " + stopWatchTest.ElapsedMilliseconds + "ms");

            Console.WriteLine(); // Line break

            Console.WriteLine("<-------------------------Result-------------------------------->");
            Console.WriteLine(); // Line break

            Console.WriteLine("Full result: " + Convert.ToBase64String(data)); // Here the finished hash is displayed in a base64 format

            Console.WriteLine(); // Line break

            Console.WriteLine("result length = " + data.Length); // The length of the data array is displayed here for bebugging
            Console.WriteLine("original text = " + input);

            Console.ReadLine();

            Environment.Exit(0);
        }

        protected static void BCRYPT() // This method is for BCRYPT
        {
            Console.Clear(); // This clears the console

            Console.Title = "SHA/BCRYPT || CURRENT = BCRYPT"; // Changes the title so that the I know I'm using bcrypt

            Console.WriteLine("<--------------------------Data input---------------------------------->");

            Console.Write("Please enter some data: "); // This tells the user to input some data
            string input = Console.ReadLine(); // This stores the users input in a variable called input

            Console.WriteLine(""); // Line break
       
            Console.WriteLine("<-------------------------Input processing-------------------------------->");

            Console.WriteLine(); // Line break

            Console.WriteLine("<-------------------------Hashing-------------------------------->");

            stopWatchTest.Start();
            
            string hash = BCrypt.Net.BCrypt.GenerateSalt();
            string fin = BCrypt.Net.BCrypt.HashPassword(input, hash);

            stopWatchTest.Stop();
            Console.WriteLine("Time taken to compute " + stopWatchTest.ElapsedMilliseconds + "ms");

            Console.WriteLine();

            Console.WriteLine("<-------------------------Result-------------------------------->");

            Console.WriteLine("The salt is " + hash);

            Console.WriteLine();

            Console.WriteLine("The hash is " + fin);

            Console.ReadLine();

            Environment.Exit(0);
        }

        /*protected static void TESTBENCH()
        {
            Console.Title = "SHA/BCRYPT || CURRENT = TESTBENCH";
            Console.Clear();

            Console.WriteLine();
            Console.WriteLine("Please enter the name and exstention of the data set you wish to use");
            string file = Console.ReadLine().ToLower();

            string[] file_type = new string[2];
            file_type = file.Split('.');

            Console.WriteLine();

            //Console.WriteLine("");

            if(file_type[1] == "txt")
            {
                Console.WriteLine("Hay!");
            }

            else
            {
                Console.WriteLine("That is not an option!");
                Environment.Exit(0);
            }

            Console.ReadLine();
        }*/
    }
}