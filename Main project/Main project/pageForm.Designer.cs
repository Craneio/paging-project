﻿namespace Main_project
{
    partial class pageForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSend = new System.Windows.Forms.Button();
            this.lblEnterName = new System.Windows.Forms.Label();
            this.txtRecipient = new System.Windows.Forms.TextBox();
            this.lblEnterMessage = new System.Windows.Forms.Label();
            this.txtMessage = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnSend
            // 
            this.btnSend.Location = new System.Drawing.Point(206, 31);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(98, 42);
            this.btnSend.TabIndex = 0;
            this.btnSend.Text = "Send";
            this.btnSend.UseVisualStyleBackColor = true;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // lblEnterName
            // 
            this.lblEnterName.AutoSize = true;
            this.lblEnterName.Location = new System.Drawing.Point(12, 20);
            this.lblEnterName.Name = "lblEnterName";
            this.lblEnterName.Size = new System.Drawing.Size(168, 13);
            this.lblEnterName.TabIndex = 1;
            this.lblEnterName.Text = "Please enter the name of recipient";
            // 
            // txtRecipient
            // 
            this.txtRecipient.Location = new System.Drawing.Point(25, 43);
            this.txtRecipient.Name = "txtRecipient";
            this.txtRecipient.Size = new System.Drawing.Size(155, 20);
            this.txtRecipient.TabIndex = 2;
            // 
            // lblEnterMessage
            // 
            this.lblEnterMessage.AutoSize = true;
            this.lblEnterMessage.Location = new System.Drawing.Point(15, 77);
            this.lblEnterMessage.Name = "lblEnterMessage";
            this.lblEnterMessage.Size = new System.Drawing.Size(134, 13);
            this.lblEnterMessage.TabIndex = 3;
            this.lblEnterMessage.Text = "Please enter your message";
            // 
            // txtMessage
            // 
            this.txtMessage.Location = new System.Drawing.Point(25, 100);
            this.txtMessage.Multiline = true;
            this.txtMessage.Name = "txtMessage";
            this.txtMessage.Size = new System.Drawing.Size(294, 175);
            this.txtMessage.TabIndex = 4;
            // 
            // pageForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(336, 295);
            this.Controls.Add(this.txtMessage);
            this.Controls.Add(this.lblEnterMessage);
            this.Controls.Add(this.txtRecipient);
            this.Controls.Add(this.lblEnterName);
            this.Controls.Add(this.btnSend);
            this.Name = "pageForm";
            this.Text = "Send a page!";
            this.Load += new System.EventHandler(this.pageForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSend;
        private System.Windows.Forms.Label lblEnterName;
        private System.Windows.Forms.TextBox txtRecipient;
        private System.Windows.Forms.Label lblEnterMessage;
        private System.Windows.Forms.TextBox txtMessage;
    }
}