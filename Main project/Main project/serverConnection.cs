﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Net;
using System.IO;
using System.Windows.Forms;
using System.Net.Sockets;

namespace Main_project
{
    public class serverConnection
    {
        private TcpClient client = new TcpClient();
        private NetworkStream server;

        private commands command = new commands();

        public bool login(string userName, string passsword)
        {
            try
            {
                client.Connect(IPAddress.Parse(GetLocalIPAddress()), 8888);
                server = client.GetStream();

                byte[] toServer = Encoding.ASCII.GetBytes(command.salt + "&" + command.userName + userName);
                server.Write(toServer, 0, toServer.Length);
                
                byte[] fromServer = new byte[1025];
                server.Read(fromServer, 0, 1025);
                string salt = Encoding.ASCII.GetString(fromServer);

                hashClass hasher = new hashClass();

                string hash = hasher.hash(salt + passsword);
                toServer = Encoding.ASCII.GetBytes(command.login + "&" + command.userName + userName + "&" + command.password + hash);
                server.Write(toServer, 0, toServer.Length);

                server.Read(fromServer, 0, 1025);
                string temp = Encoding.ASCII.GetString(fromServer);

                if (temp.Replace("\0", "") == "True")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                MessageBox.Show("Connection with server was interupted, please try again");
                return false;
            }
            finally
            {
                client.Close();
            }
        }

        public void sendMessage(string message, string recipient, string sender)
        {
            try
            {
                client.Connect(IPAddress.Parse(GetLocalIPAddress()), 8888);
                server = client.GetStream();

                byte[] toServer = Encoding.ASCII.GetBytes(command.sendPage + "&" + command.message + message + "&" + command.recipient + recipient + "&" + command.sender + sender);
                server.Write(toServer, 0, toServer.Length);

                byte[] fromServer = new byte[1025];
                server.Read(fromServer, 0, 1025);
                string salt = Encoding.ASCII.GetString(fromServer);
            }
            catch
            {
                MessageBox.Show("Connection with server was interupted, please try again");
                return;
            }
            finally
            {
                client.Close();
            }
        }

        struct commands
        {
            public string login
            {
                get
                {
                    return "login="; // Implemented fully
                }
            }
            public string userName
            {
                get
                {
                    return "uName="; // Implemented fully
                }
            }
            public string password
            {
                get
                {
                    return "password="; // Implemented fully
                }
            }
            public string salt
            {
                get
                {
                    return "salt="; // Implemented fullt
                }
            }
            public string register
            {
                get
                {
                    return "reg="; // Err kind of
                }
            }
            public string sendPage
            {
                get
                {
                    return "sendPage="; // NOT IMPLEMENTED
                }
            }
            public string message
            {
                get
                {
                    return "message="; // NOT IMPLEMENTED
                }
            }
            public string recipient
            {
                get
                {
                    return "recipient=";
                }
            }
            public string sender
            {
                get
                {
                    return "sender=";
                }
            }
        }

        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("Local IP Address Not Found!");
        }

        public void listen()
        {
            TcpListener listener = new TcpListener(IPAddress.Parse(GetLocalIPAddress()), 8889);
            
            while(true)
            {
                if (listener.Pending())
                {
                    TcpClient client = listener.AcceptTcpClient();
                    NetworkStream fromServer = client.GetStream();

                    byte[] s = new byte[client.ReceiveBufferSize];

                    fromServer.Read(s, 0, s.Length);

                    MessageBox.Show(Encoding.ASCII.GetString(s));
                }
            }
        }
    }
}