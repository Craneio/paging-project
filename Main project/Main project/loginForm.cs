﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

//using System.Text.RegularExpressions;

namespace Main_project
{
    public partial class loginForm : Form
    {
        /*
         * ^ Above are two test cast variables for ease of login when testing, the user name is DJKev and the password is
         *   1234 however it has already been hashed and has been stored as such so that it can be send straight to the
         *   server and does not have to be hashed everytime^
         */

        bool alreadyWrong = false; // Here alreadyWrong is created and initilised with false

        public loginForm()
        {
            InitializeComponent();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            serverConnection Connect = new serverConnection(); // This object is of the class serverConnection and is for server related things

            bool correctLogin = Connect.login(txtUname.Text, txtPassword.Text);
            // ^ This sets a boolean value to the return of Connect.login, it does pass the user name a return of hashOBJ.hash for the password ^

            if(correctLogin == true) // If this is true then the user has the corret login
            {
                pageForm page = new pageForm(); // Here a new form is initalised, it is for writing the pages
                page.Show(); // This shows the form pageForm, making it visible
                this.Hide(); // This hides the current form
            }
            else // This code is for if the password is not correct
            {
                if (alreadyWrong == false) // If the have already gotten their password wrong once then this is true
                {
                    foreach (Control things in this.Controls)
                    {
                        int oldX = things.Location.X; // the old X position
                        int oldY = things.Location.Y; // the old Y position
                        things.Location = new Point() // a newpoint is created
                        {
                            X = +oldX, // no offset on the X axis
                            Y = +oldY + 30 // on offset of 30 units in the Y axis
                        };

                        /*
                         * This foreach statment is for going though each control in the form and taking its current
                         * position and then offsetting its Y position by 30 units. This is so that I can get some
                         * better user feedback by soon displaying a message telling them that they are wrong
                         */ 
                    }

                    Label realPasswordPlease = new Label(); // This initiates a new label
                    realPasswordPlease.Text = "Either you password or username is wrong."; // This sets the labels text the string shown in speech marks
                    realPasswordPlease.ForeColor = Color.Red; // This sets the colour of the text to red
                    realPasswordPlease.Size = new System.Drawing.Size(208, 13); // This sets the size to be 208 units, across, and 13 units in height.
                    realPasswordPlease.Location = new Point() // Here the label is given a new point to display in
                    {
                        X = 13, // its new X position would be 13
                        Y = 13 // and its new Y position would also be 13
                    };

                    this.Size = new System.Drawing.Size(277, 240); // This sets the size of the current form to 277 units across and 240 units in height
                    this.Controls.Add(realPasswordPlease); // label is then drawn to the screen

                    alreadyWrong = true;  // alreadyWrong is set to true to indicate that they've already been wrong
                }
            }

            return;
        }
    }
}
