﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using System.Text.RegularExpressions;
using System.Data.OleDb;
using Server.Properties;

namespace Server
{
    class Program
    {
        public static TcpListener clientListener = new TcpListener(IPAddress.Parse(GetLocalIPAddress()), 8888); // The tcp listener that I'll be using to take requests

        static void Main(string[] args)
        {
            clientListener.Start(); // This starts the TCPlistener

            while (true) // This loop just keeps going and keeps checking for clients
            {
                if (clientListener.Pending() == true) // When there's a client trying to connect this is done
                {
                    Console.WriteLine("Pending connection"); // This tells me that there's currently a connection pending

                    Thread newClient = new Thread(worker); // This declares a new thread that will run the function worker
                    newClient.Start(); // This starts the new thread
                }
            }
        }
        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    Console.WriteLine(ip.ToString());
                    return ip.ToString();
                }
            }
            throw new Exception("Local IP Address Not Found!");
        }

        static void worker()
        {
            clientWorker clientGuy = new clientWorker(); // This initalises the class clientWorker called clientGuy

            Console.WriteLine(clientGuy.newConnection(ref clientListener)); // This runs the new connection fuction and passes by refereance the TCP listener that has the pending connection

            if (clientGuy.readStream() == "kill")
            {
                clientGuy.kill();
            }
        }
    }

    public class clientWorker
    {
        TcpClient clientHandler = default(TcpClient);
        NetworkStream Client;

        public string newConnection(ref TcpListener pending)
        {
            clientHandler = pending.AcceptTcpClient();
            Client = clientHandler.GetStream();

            return ((IPEndPoint)clientHandler.Client.RemoteEndPoint).Address.ToString();
        }

        public static string[,] exstractCommand(string command)
        {
            int andAmount = Regex.Matches(Regex.Escape(command), "&").Count + 1;

            // Format command=data
            string[,] CD = new string[andAmount, 1];

            int n = 0;

            string[] wow = command.Split('&');

            foreach(string s in wow)
            {
                CD[n, 0] = s.Substring(0, s.IndexOf('='));
                CD[n, 1] = s.Substring(s.IndexOf('='), s.Length - s.IndexOf('=')).Replace("=", "");

                n++;
            }

            return CD;
        }

        public string readStream()
        {
            byte[] readToEnd = new byte[1025];

            Client.Read(readToEnd, 0, 1025);

            string dataFromClient = System.Text.Encoding.ASCII.GetString(readToEnd);

            Console.WriteLine(dataFromClient);
                        
            string[,] s = exstractCommand(dataFromClient);

            for(int x = 0; x < s.GetLength(0); x++)
            {
                for(int y = 0; y < s.GetLength(1); y++)
                {
                    Console.WriteLine(s[x, y]);
                }
            }

            //Console.WriteLine(s[0, 0]);

            accessAccess access = new accessAccess();

            switch(s[0, 0])
            {
                case "login":
                    writeStream(access.login(s[1, 1], s[2, 1]).ToString());
                    break;

                case "salt":
                    writeStream(access.getSalt(s[1,1]));
                    break;

                case "reg":
                    access.register(s[1,1], s[2,1], s[3, 1]);
                    break;

                case "sendPage":
                    sendMessage(IPAddress.Parse(access.getContact(s[2, 1])), s[1, 1], s[3, 1]);
                    break;

                case "kill":
                    return "kill";

                default:
                    return "kill";
            }

            

            return "kill";
        }

        public void writeStream(string returnToClient)
        {
            byte[] ret = System.Text.Encoding.ASCII.GetBytes(returnToClient);

            Client.Write(ret, 0, ret.Length);

            return;
        }

        public void sendMessage(IPAddress recipient, string message, string sender)
        {
            if (recipient == null)
            {
                return;
            }
            else
            {
                TcpClient s = new TcpClient();
                NetworkStream c;

                s.Connect(recipient, 8888);
                c = s.GetStream();

                byte[] toRec = Encoding.ASCII.GetBytes(message + "&" + sender);

                c.Write(toRec, 0, toRec.Length);
            }
        }

        public void kill()
        {
            clientHandler.Close();
        }
    }

    public class accessAccess
    {
        const string connString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|\DB.accdb";

        OleDbConnection accessConnection = new OleDbConnection(connString);

        commandTemplates templates = new commandTemplates();

        public string getSalt(string userName)
        {
            using (OleDbConnection connection = new OleDbConnection(new Settings().DBConnectionString))
            {
                OleDbCommand command = new OleDbCommand(templates.salt + userName.Replace("\0", "") + "'", connection);
                connection.Open();
                OleDbDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    string salt = reader.GetValue(0).ToString();
                    return salt;
                }

                reader.Close();
            }
                return "";
        }

        protected string uName;
        protected string pWord;

        public bool login(string userName, string password)
        {
            try
            {
                using (OleDbConnection connection = new OleDbConnection(new Settings().DBConnectionString))
                {
                    OleDbCommand command = new OleDbCommand(templates.login + userName + "'", connection);
                    connection.Open();
                    OleDbDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        uName = reader.GetValue(0).ToString();
                        pWord = reader.GetValue(1).ToString();
                    }

                    Console.WriteLine("Given username: " + userName + " Given password: " + password);
                    Console.WriteLine("Retrived username: " + uName + " Retrived password: " + pWord);

                    if (password.Replace("\0", "") == pWord && userName.Replace("\0", "") == uName)
                    {
                        return true;
                    }
                    else
                    {
                        reader.Close();
                    }

                    return false;
                }
            }
            finally
            {
                accessConnection.Close();
            }
        }

        public void register(string userName, string password, string salt)
        {
            string query = "INSERT INTO users([uname], [pWord], [salt]) VALUES ('" + userName + "', '" + password + "', 'adas')";

            Console.WriteLine(query);

            try
            {
                using (OleDbConnection connection = new OleDbConnection(new Settings().DBConnectionString))
                {
                    OleDbCommand command = new OleDbCommand(query, connection);

                    //command.Parameters.Add("@p1", OleDbType.VarWChar).Value = userName;
                    //command.Parameters.Add("@p2", OleDbType.VarWChar).Value = password;
                    //command.Parameters.Add("@p3", OleDbType.VarWChar).Value = salt;

                    command.Connection = connection;
                    connection.Open();
                    int r = command.ExecuteNonQuery();

                    if(r > 0)
                    {
                        Console.WriteLine("Record inserted");
                    }
                    else
                    {
                        Console.WriteLine("Insertion failed");
                    }

                    /*
                    while (reader.Read())
                    {
                        uName = reader.GetValue(0).ToString();
                        pWord = reader.GetValue(1).ToString();
                    }
                    */

                    connection.Close();
                }
            }
            catch (OleDbException ex)
            {
                Console.WriteLine(ex.ToString());
                accessConnection.Close();
            }
            finally
            {
                accessConnection.Close();
            }
        }

        public string ip;

        public string getContact(string userName)
        {
            try
            {
                using (OleDbConnection connection = new OleDbConnection(new Settings().DBConnectionString))
                {
                    OleDbCommand command = new OleDbCommand(templates.getContact + userName + "'", connection);
                    connection.Open();
                    OleDbDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        ip = reader.GetValue(0).ToString();
                    }

                    Console.WriteLine("Given username: " + userName + " ip: " + ip);

                    

                    return ip;
                }
            }
            finally
            {
                accessConnection.Close();
            }
        }

        struct commandTemplates
        {
            public string salt
            {
                get
                {
                    return "SELECT salt FROM users WHERE uname = '";
                }
            }

            public string getPassword
            {
                get
                {
                    return "SELECT pWord FROM users WHERE uname = '";
                }
            }

            public string register
            {
                get
                {
                    return "INSERT INTO users(uname, pWord, salt) VALUES ( ";
                }
            }
            public string login
            {
                get
                {
                    return "SELECT uname, pWord FROM users WHERE uname = '";
                }
            }
            public string getContact
            {
                get
                {
                    return "SELECT ipAddress FROM users WHERE uname = '";
                }
            }
        }
    }
}